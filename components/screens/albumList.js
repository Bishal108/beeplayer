import React, { Component } from "react";
import {
  StyleSheet,
  FlatList,
  Dimensions,
  Modal,
  View,
  StatusBar
} from "react-native";

import GridView from "./gridView";
import ModalList from "./modalListAlbum";

const itemWidth = Dimensions.get("window").width;

export default class AlbumList extends Component {
  state = {
    songsArray: [],
    visible: false,
    index: ""
  };

  componentDidMount() {
    this.arrangeAlbum();
  }

  arrangeAlbum = async () => {
    let albumArray = [],
      temp = [],
      repeatArr = null;
    songStorage = [].concat(this.props.songsArray).sort((a, b) => {
      let song1 = a.album == null ? "unknown" : a.album.toLowerCase();
      let song2 = b.album == null ? "unknown" : b.album.toLowerCase();
      if (song1 < song2) {
        return -1;
      }
      if (song1 > song2) {
        return 1;
      }
      return 0;
    });
    for (let i = 0; i < songStorage.length; i++) {
      if (songStorage[i].album != repeatArr || i == 0) {
        repeatArr = songStorage[i].album;
        temp.push(songStorage[i]);
        for (let j = 0; j < songStorage.length; j++) {
          if (i != j && songStorage[i].album == songStorage[j].album) {
            temp.push(songStorage[j]);
          }
        }
        albumArray.push(temp);
        temp = [];
      }
    }
    this.setState({ songsArray: albumArray });
  };

  modalFn = data => {
    this.setState({ visible: true, index: data });
  };

  modalVisibility = bool => {
    this.setState({ visible: bool });
  };

  render() {
    return (
      <View style={styles.container}>
        <StatusBar backgroundColor="grey" barStyle="light-content" />
        <Modal
          transparent={false}
          visible={this.state.visible}
          onRequestClose={() => this.modalVisibility(false)}
        >
          <ModalList
            modalData={this.state.songsArray[this.state.index]}
            modalVisibility={this.modalVisibility}
            playTrack={this.props.playTrack}
          />
        </Modal>

        <FlatList
          numColumns={2}
          style={styles.container}
          onScroll={event => {
            let scrollY = event.nativeEvent.contentOffset.y;
            this.props.onScroll(scrollY);
          }}
          ListFooterComponent={
            <View style={{ height: 0, marginBottom: "18%" }} />
          }
          data={this.state.songsArray}
          renderItem={({ item, index }) => (
            <GridView
              itemWidth={itemWidth}
              item={item}
              index={index}
              modalFn={this.modalFn}
            />
          )}
          keyExtractor={item => item[0].id}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white"
  }
});
